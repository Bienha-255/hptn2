<%@ page import="model.DonHang" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.util.Random" %>
<%@ page import="dao.ThongKeDoanhThuPhimDAO" %>
<%@ page import="model.TKDTPhim" %><%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 13/11/2021
  Time: 1:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Đơn hàng</title>
    <link rel="stylesheet" href="css/bulma.min.css">
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
<div class="columns has-background-white-ter" style="margin: 0;padding: 0">
    <div class="column is-one-fifth" style="width: 230px;border-right:solid #c5c6c7 1px;background-color: #001f2b;color:#ffffff;min-height: 100vh">
        <aside class="menu">
            <a href="gdChinh.jsp"><p style="font-size: 25px; font-weight: bold">PTIT CENIMA</p></a>
            <p style="font-size: 20px">Danh mục</p>
            <ul class="menu-list">
                <li class="pl-2"><a>Nhân viên</a></li>
                <li class="pl-2"><a>Khách hàng</a></li>
                <li class="pl-2"><a>Hoá đơn</a></li>
                <li class="pl-2"><a>Kho Phim</a></li>
                <li class="pl-2"><a>Dịch vụ</a></li>
            </ul>
            <p style="font-size: 20px">Thống kê</p>
            <ul class="menu-list">
                <li class="pl-2"><a href="gdThongKeDoanhThuPhim.jsp">Thống kê doanh thu phim</a></li>
                <li class="pl-2"><a href="gdThongKeDoanhThuPhim.jsp">Thống kê doanh thu rạp</a></li>
            </ul>
        </aside>
    </div>
    <div class="column">
        <div class="container is-fluid mt-1 p-1 pt-4">
            <div class="columns is-centered">
                <div class="column box">
                    <form class="columns is-three-quarters p-4" action="gdThongKeDoanhThuPhim.jsp" method="post">
                        <label for="startDay">Ngày bắt đầu:</label>
                        <label style="width: 20px" ></label>
                        <input type="date" id="startDay" name="startDate">
                        <label style="width: 50px" ></label>
                        <label for="endDay">Ngày kết thúc:</label>
                        <label style="width: 20px" ></label>
                        <input type="date" id="endDay" name="endDate">
                        <label style="width: 50px" ></label>
                        <script>
                            $('#startDay , #endDay').datepicker({
                                dateFormat: 'dd-mm-yy'
                            });
                        </script>
                        <input type="submit" onclick="myFunction()" class="button is-info column is-1" style="width: 150px" value="Tìm kiếm"></input>
                    </form>
                    <div class="p-4">
                        <%
                            String startDay =request.getParameter("startDate");
                            String endDay =request.getParameter("endDate");
                            if (startDay!=null && endDay != null){
                                ThongKeDoanhThuPhimDAO thongKeDoanhThuPhimDAO = new ThongKeDoanhThuPhimDAO();
                                List<TKDTPhim> tkdtPhims = thongKeDoanhThuPhimDAO.getThongKeDoanhThuPhim(startDay,endDay);
                        %>
                        <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                                <thead>
                                <tr>
                                    <th class="has-text-left" style="width: 120px">Mã phim</th>
                                    <th class="has-text-centered" style="width: 120px">Tên phim</th>
                                    <th class="has-text-left" style="width: 250px">Số lượng</th>
                                    <th class="has-text-right" style="width: 50px">Tổng doanh thu</th>
                                    <th class="has-text-left" style="width: 50px">Lựa chọn</th>
                                </tr>
                                </thead>
                                <tbody>
                                <%
                                    for (TKDTPhim tkdtPhim : tkdtPhims){


                                        %>
                                <tr>
                                    <td class="has-text-left"> <%=tkdtPhim.getIdPhim()%></td>
                                    <td class="has-text-centered"> <%=tkdtPhim.getTenPhim()%></td>
                                    <td class="has-text-left"> <%=tkdtPhim.getSoLuong()%></td>
                                    <td class="has-text-right"> <%=new DecimalFormat("###,###,###").format(tkdtPhim.getDoanhThu())%></td>
                                    <td><a <%%> class="button" href="gdThongKeDoanhThuChiTietPhim.jsp?idPhim=<%=tkdtPhim.getIdPhim()%>&startDay=<%=startDay%>&endDay=<%=endDay%>" style="background-color: #dbdbdb" >Chọn</a></td>
                                </tr>
                                <%}%>
                                </tbody>
                            </table>
                        <%
                            }
                            else {
                        %>
                        <script>
                            function myFunction() {
                                alert("Hãy chọn thời gian");
                            }
                        </script>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
