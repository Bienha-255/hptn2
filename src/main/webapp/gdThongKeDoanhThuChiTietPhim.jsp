<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 13/11/2021
  Time: 3:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" import="dao.DAO" %>
<%@ page import="java.util.List" %>
<%@ page import="dao.ThongKeDoanhThuChiTietPhimDAO" %>
<%@ page import="model.TKDTChiTietPhim" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DecimalFormat" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>PTIT CENIMA</title>
    <link rel="stylesheet" href="css/bulma.min.css"/>
    <link rel="stylesheet" href="css/app.css"/>
</head>
<body>
<div class="columns has-background-white-ter" style="margin: 0;padding: 0">
    <div class="column is-one-fifth" style="width: 230px;border-right:solid #c5c6c7 1px;height: 100vh;background-color: #001f2b;color:#ffffff">
        <aside class="menu">
            <a href="gdChinh.jsp"><p style="font-size: 25px; font-weight: bold">PTIT CENIMA</p></a>
            <p style="font-size: 20px">Danh mục</p>
            <ul class="menu-list">
                <li class="pl-2"><a>Nhân viên</a></li>
                <li class="pl-2"><a>Khách hàng</a></li>
                <li class="pl-2"><a>Hoá đơn</a></li>
                <li class="pl-2"><a>Kho hàng</a></li>
                <li class="pl-2"><a>Dịch vụ</a></li>
            </ul>
            <p style="font-size: 20px">Thống kê</p>
            <ul class="menu-list">
                <li class="pl-2"><a href="gdThongKeDoanhThuPhim.jsp">Thống kê doanh thu phim</a></li>
                <li class="pl-2"><a href="gdThongKeDoanhThuPhim.jsp">Thống kê doanh thu rạp</a></li>
            </ul>
        </aside>
    </div>
    <div class="column">
        <div class="container is-fluid mt-1 p-1 pt-4">
            <div class="columns is-centered">
                <div class="column">
                    <div class="p-4">
                        <%
                            String idPhim = request.getParameter("idPhim");
                            String startDay = request.getParameter("startDay");
                            String endDay = request.getParameter("endDay");
                            if (idPhim!=null){
                                ThongKeDoanhThuChiTietPhimDAO thongKeDoanhThuChiTietPhimDAO = new ThongKeDoanhThuChiTietPhimDAO();
                                List<TKDTChiTietPhim> tkdtChiTietPhims = thongKeDoanhThuChiTietPhimDAO.getThongKeDoanhThuChiTietPhim(idPhim,startDay,endDay);
                                System.out.println(tkdtChiTietPhims);
                        %>
                        <div class="box">
                            <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                                <thead>
                                <tr>
                                    <th class="has-text-left" style="width: 120px">Giờ chiếu</th>
                                    <th class="has-text-left" style="width: 250px">Ngày chiếu</th>
                                    <th class="has-text-left" style="width: 250px">Tổng vé bán ra</th>
                                    <th class="has-text-right" style="width: 120px">Số tiền thu được</th>
                                </tr>
                                </thead>
                                <tbody>
                                <%
                                    for (TKDTChiTietPhim i : tkdtChiTietPhims){ %>
                                <tr>
                                    <td class="has-text-left"> <%=i.getGioChieu()%></td>
                                    <td class="has-text-left"> <%=new SimpleDateFormat("dd/MM/yyyy").format(i.getNgayChieu())%></td>
                                    <td class="has-text-left"> <%=i.getTongVeBan()%></td>
                                    <td class="has-text-right"> <%=new DecimalFormat("###,###,###").format(i.getSoTien())%></td>
                                </tr>
                                <%}%>
                                </tbody>
                            </table>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

