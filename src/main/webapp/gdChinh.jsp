<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" import="dao.DAO" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>PTIT CENIMA</title>
    <link rel="stylesheet" href="css/bulma.min.css">
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <div class="columns has-background-white-ter" style="margin: 0;padding: 0">
        <div class="column is-one-fifth" style="width: 230px;border-right:solid #c5c6c7 1px;height: 100vh;background-color: #001f2b;color:#ffffff">
            <aside class="menu">
                <a href="gdChinh.jsp"><p style="font-size: 25px; font-weight: bold">PTIT CENIMA</p></a>
                <p style="font-size: 20px">Danh mục</p>
                <ul class="menu-list">
                    <li class="pl-2"><a>Nhân viên</a></li>
                    <li class="pl-2"><a>Hoá đơn</a></li>
                    <li class="pl-2"><a>Kho Phim</a></li>
                    <li class="pl-2"><a>Dịch vụ</a></li>
                </ul>
                <p style="font-size: 20px">THỐNG KÊ</p>
                <ul class="menu-list">
                    <li class="pl-2"><a href="gdThongKeDoanhThuPhim.jsp">Thống kê doanh thu phim</a></li>
                    <li class="pl-2"><a href="gdThongKeDoanhThuPhim.jsp">Thống kê doanh thu rạp</a></li>
                </ul>
            </aside>
        </div>
        <div class="column">
            <div class="columns is-centered">
                <div class="column is-half">
                    <img src="img/movie.svg" alt="">
                </div>
            </div>
        </div>
    </div>
</body>
</html>
