package dao;

import model.TKDTChiTietPhim;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ThongKeDoanhThuChiTietPhimDAO extends DAO{
    public ThongKeDoanhThuChiTietPhimDAO() {
        super();
    }

    public List<TKDTChiTietPhim> getThongKeDoanhThuChiTietPhim(String idPhim, String startDay, String endDay) {
        List<TKDTChiTietPhim> lkdhs = new ArrayList<>();
        String query = "SELECT lc.thoiGianChieu as gioChieu,lc.ngayChieu as ngayChieu, sum(v.soluong) as soVeBan,sum(v.totalPrice) as soTien\n" +
                "                FROM lich_chieu lc\n" +
                "                LEFT JOIN ve v on lc.id = v.idLichChieu " +
                "where lc.idPhim = '" + idPhim + "' and v.created >= '"+ startDay + "' and v.created <= '"+ endDay+"' group by lc.thoiGianChieu,lc.ngayChieu";
        System.out.println(query);
        try (
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query)
        ) {
            while(rs.next()){
                //Linh kien
                TKDTChiTietPhim lk = new TKDTChiTietPhim();
                lk.setGioChieu(rs.getString("gioChieu"));
                lk.setNgayChieu(rs.getDate("ngayChieu"));
                lk.setTongVeBan(rs.getFloat("soVeBan"));
                lk.setSoTien(rs.getFloat("soTien") * rs.getFloat("soVeBan"));
                lkdhs.add(lk);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return lkdhs;
    }
}
