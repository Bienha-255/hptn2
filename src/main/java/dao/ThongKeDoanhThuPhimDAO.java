package dao;

import model.TKDTPhim;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ThongKeDoanhThuPhimDAO extends DAO {
    public ThongKeDoanhThuPhimDAO() {
        super();
    }

    public List<TKDTPhim> getThongKeDoanhThuPhim(String startDate, String endDate) {
        List<TKDTPhim> tkdtPhims = new ArrayList<>();
        String query = "SELECT p.id as maPhim,p.ten as tenPhim,sum(v.soluong) as soLuong,sum(v.totalPrice) as doanhThu \n" +
                "FROM phim p \n" +
                "LEFT JOIN ve v on v.idPhim = p.id\n" +
                "WHERE v.created >= '" + startDate + "' and v.created <= '"+ endDate + "' group  by v.idPhim";
        try (
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query)
        ) {
            while(rs.next()){
                //DonHang
                TKDTPhim tk = new TKDTPhim();
                tk.setIdPhim(rs.getString("maPhim"));
                tk.setTenPhim(rs.getString("tenPhim"));
                tk.setSoLuong(rs.getFloat("soLuong"));
                tk.setDoanhThu(rs.getFloat("doanhThu")*rs.getFloat("soLuong"));

                tkdtPhims.add(tk);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return tkdtPhims;
    }
}
