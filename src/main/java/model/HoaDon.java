package model;

import lombok.Data;

import java.util.Date;

@Data
public class HoaDon {
    private String id;
    private Date created;
    private Float tongTien;
    private String idDonHang;
    private String idKhachHang;
}
