package model;

import lombok.Data;

import java.util.Date;

@Data
public class TKDTChiTietPhim {
    private String gioChieu;
    private Date ngayChieu;
    private Float tongVeBan;
    private Float soTien;
}
