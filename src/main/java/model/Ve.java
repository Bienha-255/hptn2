package model;

import lombok.Data;

@Data
public class Ve {
    private String id;
    private String idDonHang;
    private Float donGia;
    private int soGhe;
}
