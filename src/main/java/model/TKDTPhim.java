package model;

import lombok.Data;

@Data
public class TKDTPhim {
    private String idPhim;
    private String tenPhim;
    private Float soLuong;
    private Float doanhThu;
}
