package model;

import lombok.Data;

import java.util.Date;

@Data
public class DonHang {
    private String id;
    private Date createdDate;
    private String idVe;
    private String idNhanVien;
    private Float soLuong;
    private Float totalPrice;
    private String note;
}
