package model;

import lombok.Data;

import java.util.Date;

@Data
public class Phim {
    private String id;
    private String ten;
    private String loaiPhim;
    private Date ngaySanXuat;
    private float thoiGian;
}
