package model;

import lombok.Data;

import java.util.Date;

@Data
public class LichChieu {
    private String id;
    private String idVe;
    private String idPhim;
    private Date thoiGianChieu;
}
