
CREATE TABLE `ve` (
                      `id` varchar(10) DEFAULT NULL,
                      `created` date DEFAULT NULL,
                      `totalPrice` double DEFAULT NULL,
                      `idPhim` varchar(255) DEFAULT NULL,
                      `idNhanVien` varchar(255) DEFAULT NULL,
                      `soLuong` double DEFAULT NULL,
                      `note` varchar(10) DEFAULT NULL,
                      `idLichChieu` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `phim` (
                        `id` varchar(10) DEFAULT NULL,
                        `ten` nvarchar(255) DEFAULT NULL,
                        `loaiPhim` nvarchar(255) DEFAULT NULL,
                        `ngaySanXuat` date DEFAULT NULL,
                        `thoigian` double DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `lich_chieu` (
                              `id` varchar(10) DEFAULT NULL,
                              `idVe` varchar(10) DEFAULT NULL,
                              `idPhim` varchar(10) DEFAULT NULL,
                              `thoiGianChieu` varchar(255) DEFAULT NULL,
                              `ngayChieu` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
