package MainTest;

import dao.DAO;
import dao.ThongKeDoanhThuChiTietPhimDAO;
import dao.ThongKeDoanhThuPhimDAO;
import model.TKDTChiTietPhim;
import model.TKDTPhim;
import org.junit.Test;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MainTest {

    @Test
    public void testOK() throws SQLException {
        // them data test
        initData();

        // expected
        String startDay = "2021-09-01";
        String endDay = "2021-09-07";

        // actual
        ThongKeDoanhThuPhimDAO thongKeDoanhThuPhimDAO = new ThongKeDoanhThuPhimDAO();
        List<TKDTPhim> tkdtPhims = thongKeDoanhThuPhimDAO.getThongKeDoanhThuPhim(startDay,endDay);

        // check
        // xem ds tìm được có giống mong muốn hay không
        assertTrue(tkdtPhims.get(0).getTenPhim().equals("Texas"));

        // revert data
        String deleteStatement = "delete from phim where id = ?";
        String deleteStatement2 = "delete from ve where id = ?";
        String deleteStatement3 = "delete from lich_chieu where id = ?";
        DAO dao = new DAO();
        PreparedStatement preparedStmt = dao.con.prepareStatement(deleteStatement);
        PreparedStatement preparedStmt2 = dao.con.prepareStatement(deleteStatement2);
        PreparedStatement preparedStmt3 = dao.con.prepareStatement(deleteStatement3);
        preparedStmt.setString(1, "P001");
        preparedStmt2.setString(1,"V001");
        preparedStmt3.setString(1,"L001");
    }

    @Test
    public void testNullDate() throws SQLException {
        // them data test
        initData();

        // expected
        String startDay = null;
        String endDay = "2021-09-07";


        // check
        // xem ds tìm được có giống mong muốn hay không
        assertFalse("Hãy chọn thời gian",false);

        // revert data
        String deleteStatement = "delete from phim where id = ?";
        String deleteStatement2 = "delete from ve where id = ?";
        String deleteStatement3 = "delete from lich_chieu where id = ?";
        DAO dao = new DAO();
        PreparedStatement preparedStmt = dao.con.prepareStatement(deleteStatement);
        PreparedStatement preparedStmt2 = dao.con.prepareStatement(deleteStatement2);
        PreparedStatement preparedStmt3 = dao.con.prepareStatement(deleteStatement3);
        preparedStmt.setString(1, "P001");
        preparedStmt2.setString(1,"V001");
        preparedStmt3.setString(1,"L001");
    }

//    @Test
//    public void getChiTiet() throws SQLException {
//        // them data test
//        initData();
//
//        // expected
//        String maPhim = "P001";
//        String startDay = "2021-09-01";
//        String endDay = "2021-12-22";
//
//        // actual
//        ThongKeDoanhThuChiTietPhimDAO thongKeDoanhThuChiTietPhimDAO = new ThongKeDoanhThuChiTietPhimDAO();
//        List<TKDTChiTietPhim> tkdtChiTietPhims = thongKeDoanhThuChiTietPhimDAO.getThongKeDoanhThuChiTietPhim(maPhim,startDay,endDay);
//
//        // check
//        // xem ds tìm được có giống mong muốn hay không
//        assertTrue(tkdtChiTietPhims.get(0).getGioChieu().equals("14h"));
//
//        // revert data
//        String deleteStatement = "delete from phim where id = ?";
//        String deleteStatement2 = "delete from ve where id = ?";
//        String deleteStatement3 = "delete from lich_chieu where id = ?";
//        DAO dao = new DAO();
//        PreparedStatement preparedStmt = dao.con.prepareStatement(deleteStatement);
//        PreparedStatement preparedStmt2 = dao.con.prepareStatement(deleteStatement2);
//        PreparedStatement preparedStmt3 = dao.con.prepareStatement(deleteStatement3);
//        preparedStmt.setString(1, "P001");
//        preparedStmt2.setString(1,"V001");
//        preparedStmt3.setString(1,"L001");
//    }


    public void initData() throws SQLException {
        DAO dao = new DAO();
        PreparedStatement statement = null;
        // insert DH
        String inSertTKDT = " insert into ve (id, created, totalPrice, idPhim, soluong,note,idLichChieu)"
                + " values (?, ?, ?, ?, ?,?,?)";
        statement = dao.con.prepareStatement(inSertTKDT);
        statement.setString(1, "V001");
        statement.setString(2, "2021-12-20");
        statement.setFloat(3, 100000f);
        statement.setString(4, "P001");
        statement.setFloat(5, 2f);
        statement.setString(6,"note");
        statement.setString(7,"L001");
        statement.execute();


        // insert KH
        String insertPhim = " insert into phim (id, ten,loaiPhim,ngaySanXuat,thoigian)"
                + " values (?, ?,?,?,?)";
        statement = dao.con.prepareStatement(insertPhim);
        statement.setString(1, "P001");
        statement.setString(2, "Texas");
        statement.setString(3,"action");
        statement.setDate(4,new Date(System.currentTimeMillis()));
        statement.setString(5,"2");


        // insert KH
        String insertLichChieu = " insert into lich_chieu (id, idPhim,thoiGianChieu,ngayChieu)"
                + " values (?,?,?,?)";
        statement = dao.con.prepareStatement(insertLichChieu);
        statement.setString(1, "L001");
        statement.setString(2, "P001");
        statement.setString(3,"14h");
        statement.setString(4,"2021-12-22");



    }
}
